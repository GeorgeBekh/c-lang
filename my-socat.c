#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <string.h>
#include <stdlib.h>

#define BUFF_SIZE 32
#define MAX_ADDR_LENGTH 50

int create_listening_socket(char *addr, int port);

int main(int argc, char *argv[])
{
  int fd_sock_listen, fd_sock_connect, new_sock_fd, new_conn_fd, port_from, port_to, bytes_c;
  socklen_t client, server;
  char addr_from[MAX_ADDR_LENGTH];
  char addr_to[MAX_ADDR_LENGTH];
  char buffer[BUFF_SIZE];

  if (argc < 5) {
    printf("Usage: %s host port to_host to_port\n", argv[0]);

    return 1;
  }

  port_from = atoi(argv[2]);
  strncpy(addr_from, argv[1], MAX_ADDR_LENGTH - 1);
  port_to = atoi(argv[3]);
  strncpy(addr_to, argv[4], MAX_ADDR_LENGTH - 1);

  fd_sock_listen = create_listening_socket(addr_from, port_from);

  printf("PID %d\n", getpid());
  printf("fd: %d\n", fd_sock_listen);

  struct sockaddr_in cli_addr;
  client = sizeof(cli_addr);

  printf("Listening\n");

  while ((new_sock_fd = accept(fd_sock_listen, (struct sockaddr *) &cli_addr, &client)) >= 0) {
      bzero(buffer, BUFF_SIZE);

      if ((fd_sock_connect = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
        printf("Error creating socket\n");
        exit(1);
      }

      struct sockaddr_in to_serv_addr = { .sin_family = AF_INET, .sin_addr.s_addr = inet_addr(addr_to), .sin_port = htons(port_to) };
      server = sizeof(to_serv_addr);

      new_conn_fd = connect(fd_sock_connect, (struct sockaddr *) &to_serv_addr, server);

      while ((bytes_c = read(new_sock_fd, buffer, BUFF_SIZE)) != 0) {
        if (bytes_c < 0) {
          printf("Error reading\n");

          return 1;
        }

        write(STDOUT_FILENO, buffer, bytes_c);

        //bytes_c = write(new_sock_fd, buffer, bytes_c);
      }

      close(new_sock_fd);
  }

  printf("Error accepting\n");
  close(new_sock_fd);
  close(fd_sock_listen);

  return 1;
}

int create_listening_socket(char *addr, int port)
{
  int fd;

  if ((fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
    printf("Error creating socket\n");

    exit(1);
  }

  setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (int *) 1, sizeof(int));

  struct sockaddr_in serv_addr = { .sin_family = AF_INET, .sin_addr.s_addr = inet_addr(addr), .sin_port = htons(port) };

  if (bind(fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    printf("Error binding\n");

    exit(1);
  }

  listen(fd, 5);

  return fd;
}
