#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

#define BUFF_SIZE 1024

int main(int argc, char *argv[])
{
  int fd, i, bytes_c;

  char buff[BUFF_SIZE];

  if (argc < 2) {
    printf("Usage: %s filename\n", argv[0]);

    return 1;
  }

  if ((fd = open(argv[1], O_RDONLY)) == -1) {
    printf("Error: %d\n", errno);

    if (errno == ENOENT) {
      printf("File doesn't exist\n");
    }

    return errno;
  }

  while ((bytes_c = read(fd, &buff, BUFF_SIZE)) > 0) {
    write(STDOUT_FILENO, &buff, bytes_c);
  }

  close(fd);

  return 0;
}
